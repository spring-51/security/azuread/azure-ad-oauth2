package poc.spring.oauth2.azureAD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzureAdOauth2Application {

	public static void main(String[] args) {
		SpringApplication.run(AzureAdOauth2Application.class, args);
	}

}
