package poc.spring.oauth2.azureAD.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
// ref https://dev.to/azure/using-spring-security-with-azure-active-directory-mga
public class AzureADLoginController {

	// The redirect URI must be "http://localhost:8080/login/oauth2/code/azure"
	// as per https://dev.to/azure/using-spring-security-with-azure-active-directory-mga
	@GetMapping
	public String login() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(authentication);
		String name = ((OAuth2AuthenticationToken)authentication).getPrincipal().getAttribute("email");
		return name;
	}
}
